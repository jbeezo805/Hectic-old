// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../persistence/app_database.dart';
import '../persistence/plan.dart';
import '../utils/edit_utils.dart';
import '../utils/plan_reminder.dart';
import '../utils/strings.dart';
import '../utils/time.dart';
import 'edit_plan.dart';
import 'planner.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final PlanReminder _planReminder = PlanReminder();
  final Time _time = Time();
  final AppDatabase _appDb = AppDatabase.get();
  bool _is24HourFormat = false;
  int _jumpDate;
  int _todayDate;

  @override
  void initState() {
    super.initState();

    _setDateToday();
    _getIf24HourFormat();
  }

  void _getIf24HourFormat() async {
    final bool result = await _time.is24HourFormat();
    setState(() {
      _is24HourFormat = result;
    });
  }

  void _setDateToday() {
    final now = DateTime.now();
    final year = now.year;
    final month = now.month;
    final date = now.day;

    setState(() {
      _todayDate = DateTime(year, month, date).millisecondsSinceEpoch;
      _jumpDate = _todayDate;
    });
  }

  Future<List<Plan>> _getTodayPlans() async {
    return await _appDb.getPlansByDate(_jumpDate);
  }

  Future _addPlan() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditPlan(
              title: Strings.makePlan,
              plan: Plan(),
              editMode: EditUtils.newData,
              jumpDate: _jumpDate,
              todayDate: _todayDate,
            ),
      ),
    );

    if (result != null) {
      var newPlan = Plan.fromMap(result);
      newPlan.dateCreated = _todayDate;

      final insertedPlan = await _appDb.insertPlan(newPlan);

      if (newPlan.isReminderSet) {
        final reminderSchedule =
            DateTime.fromMillisecondsSinceEpoch(newPlan.reminderSchedule);
        _planReminder.schedule(
            insertedPlan.id, newPlan.details, reminderSchedule);
      }

      _showSnackBar(Strings.planSaved);
    }
  }

  Future _handlePlanUpdate(Plan plan) async {
    await _appDb.updatePlan(plan);
    _showSnackBar(Strings.planUpdated);
  }

  void _cancelReminder(int id) {
    _planReminder.cancel(id);
  }

  void _setReminder(Plan plan) {
    var reminderSchedule =
        DateTime.fromMillisecondsSinceEpoch(plan.reminderSchedule);
    _planReminder.schedule(plan.id, plan.details, reminderSchedule);
  }

  Future _handlePlanDeletion(int id) async {
    if (id != null) {
      int result = await _appDb.deletePlan(id);
      if (result >= 0) _showSnackBar(Strings.planDeleted);
    }
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(text)));
  }

  FutureBuilder _displayPlanner() {
    return FutureBuilder(
      future: _getTodayPlans(),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data.isNotEmpty) {
          return Planner(
            plans: snapshot.data,
            jumpDate: _jumpDate,
            todayDate: _todayDate,
            onChange: _handlePlanUpdate,
            onDelete: _handlePlanDeletion,
            cancelReminder: _cancelReminder,
            setReminder: _setReminder,
            is24HourFormat: _is24HourFormat,
          );
        }

        return Container(
          alignment: Alignment.center,
          child: Text(
            Strings.noPlansShown,
            style: TextStyle(
              fontSize: 18.0,
            ),
          ),
        );
      },
    );
  }

  Future _pickJumpDate(BuildContext context) async {
    DateTime initialDate = DateTime.now();
    DateTime firstDate = initialDate.subtract(Duration(days: 1825));
    DateTime lastDate = initialDate.add(Duration(days: 1825));

    DateTime result = await showDatePicker(
        context: context,
        initialDate: DateTime.fromMillisecondsSinceEpoch(_jumpDate),
        firstDate: firstDate,
        lastDate: lastDate);

    setState(() {
      _jumpDate = DateTime(result.year, result.month, result.day)
          .millisecondsSinceEpoch;
    });
  }

  @override
  Widget build(BuildContext context) {
    final String dateToShow =
        '${DateFormat.yMMMd().format(DateTime.fromMillisecondsSinceEpoch(_jumpDate))}';

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.calendar_today),
          onPressed: () {
            _pickJumpDate(context);
          },
        ),
        title: Text(dateToShow),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Navigator.of(context).pushNamed('/settings');
            },
          ),
        ],
      ),
      body: _displayPlanner(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _addPlan();
        },
        tooltip: Strings.makePlan,
        child: Icon(Icons.add),
      ),
    );
  }
}
