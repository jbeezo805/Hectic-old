// Copyright (C) 2018  Kent Delante

// This file is part of Hectic

// Hectic is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Hectic is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Hectic. If not, see <http://www.gnu.org/licenses/>.

import "dart:async";

import "package:flutter/services.dart";

class Time {
  static const platform =
      MethodChannel("com.gitlab.kdeleteme.hectic/hourformat");

  Future<bool> is24HourFormat() async {
    bool result = await platform.invokeMethod("getIf24HourFormat");

    return result;
  }
}
